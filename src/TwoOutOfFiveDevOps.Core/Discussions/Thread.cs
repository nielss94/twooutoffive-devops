using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.Backlogs.States;
using TwoOutOfFiveDevOps.Core.Observers;
using TwoOutOfFiveDevOps.Core.Teams;

namespace TwoOutOfFiveDevOps.Core.Discussions
{
    //  Observer pattern implemented
    public class Thread: IObserver
    {
        public string Subject { get; private set; }
        public Person Owner { get; private set; }
        public string Title { get; private set; }
        public string Content { get; private set; }
        public bool Closed { get; private set; }

        private BacklogTask backlogTask;
        private BacklogTask BacklogTask
        {
            get => backlogTask;
            set
            {
                backlogTask?.RemoveObserver(this);
                
                backlogTask = value;
                backlogTask.RegisterObserver(this);
            }
        }

        private readonly List<Comment> comments;

        private Thread(BacklogTask backlogTask)
        {
            BacklogTask = backlogTask;
            comments = new List<Comment>();
        }

        public void AddComment(Comment comment)
        {
            if (Closed) return;
            
            comment.Thread = this;
            comments.Add(comment);
        }

        public Comment GetComment(int index)
        {
            return comments.ElementAtOrDefault(index);
        }

        public void RemoveComment(Comment comment)
        {
            comments.Remove(comment);
        }
        
        public void Update(IState state = null)
        {
            BacklogTaskState currentState = (BacklogTaskState) BacklogTask.GetState();
            if (!Closed)
            {
                Closed = currentState.GetType() == typeof(BacklogFinishedState);
            }
            else
            {
                Closed = currentState.GetType() != typeof(BacklogTaskCreatedState);
            }
        }
        
        //Builder pattern implemented
        public class Builder
        {
            private readonly Thread thread;
            
            public Builder(BacklogTask backlogTask)
            {
                thread = new Thread(backlogTask);
            }

            public Builder WithSubject(string subject)
            {
                thread.Subject = subject;
                return this;
            }

            public Builder WithOwner(Person owner)
            {
                thread.Owner = owner;
                return this;
            }

            public Builder WithTitle(string title)
            {
                thread.Title = title;
                return this;
            }

            public Builder WithContent(string content)
            {
                thread.Content = content;
                return this;
            }

            public Builder WithComments(IEnumerable<Comment> comments)
            {
                foreach (Comment comment in comments)
                {
                    thread.AddComment(comment);
                }

                return this;
            }

            public Builder WithComment(Comment comment)
            {
                thread.AddComment(comment);
                return this;
            }
            
            public Thread Build()
            {
                return thread;
            }
        }
    }
}
