﻿using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.Teams;

namespace TwoOutOfFiveDevOps.Core.Discussions
{
    public class Comment
    {
        public string Content { get; }
        public Person Owner { get; }
        public Thread Thread { private get; set; }

        private readonly List<Comment> comments;

        public Comment(string content, Person owner)
        {
            Content = content;
            Owner = owner;
            comments = new List<Comment>();
        }

        public void AddChild(Comment comment)
        {
            if(Thread != null)
            {
                if (Thread.Closed) return;
                comment.Thread = Thread;
            }
            
            comments.Add(comment);
        }

        public void RemoveChild(Comment comment)
        {
            comments.Remove(comment);
        }

        public Comment GetChild(int index)
        {
            return comments.ElementAtOrDefault(index);
        }

        public int GetNumberOfComments()
        {
            return comments.Count;
        }
    }
}
