using System.Collections.Generic;
using System.Linq;

namespace TwoOutOfFiveDevOps.Core.Discussions
{
    public class Forum
    {
        private readonly List<Thread> threads;
        
        public Forum()
        {
            threads = new List<Thread>();
        }

        public void AddThread(Thread thread)
        {
            threads.Add(thread);
        }

        public void RemoveThread(Thread thread)
        {
            threads.Remove(thread);
        }

        public Thread GetThread(int index)
        {
            return threads.ElementAtOrDefault(index);
        }

        public List<Thread> GetThreads()
        {
            return threads;
        }
    }
}
