﻿using System;

namespace TwoOutOfFiveDevOps.Core.Sprints.Reports
{
    //Template method pattern
    public class PdfSprintReport : SprintReport
    {
        public PdfSprintReport(Sprint sprint) : base(sprint)
        {
        }

        public override void WriteReport()
        {
            Console.WriteLine("Writing sprint report to PDF...");
        }
    }
}
