﻿using System;

namespace TwoOutOfFiveDevOps.Core.Sprints.Reports
{
    //Template method pattern
    public class PngSprintReport : SprintReport
    {
        public PngSprintReport(Sprint sprint) : base(sprint)
        {
        }

        public override void WriteReport()
        {
            Console.WriteLine("Writing sprint report to PNG...");
        }
    }
}
