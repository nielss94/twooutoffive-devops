﻿using System;

namespace TwoOutOfFiveDevOps.Core.Sprints.Reports
{
    //Template method pattern
    public abstract class SprintReport
    {
        public Sprint Sprint { get; private set; }

        protected SprintReport(Sprint sprint)
        {
            Sprint = sprint;
        }

        public void GenerateReport()
        {
            CreateHeader();
            CreateBody();
            CreateFooter();
            WriteReport();
        }

        private void CreateHeader()
        {
            Console.WriteLine("Creating report header...");
        }

        public virtual void CreateBody()
        {
            Console.WriteLine("Creating report body...");
        }

        private void CreateFooter()
        {
            Console.WriteLine("Creating report footer...");
        }

        public abstract void WriteReport();
    }
}
