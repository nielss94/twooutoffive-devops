﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.Factory
{
    //Factory pattern
    public interface ISprintFactory
    {
        Sprint CreateSprint(string type, DateTime? startDate, DateTime? endDate);
    }
}
