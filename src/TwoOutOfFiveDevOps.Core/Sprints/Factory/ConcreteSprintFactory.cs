﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.Factory
{
    //Factory pattern
    public class ConcreteSprintFactory : ISprintFactory
    {
        public Sprint CreateSprint(string type, DateTime? startDate , DateTime? endDate)
        {
            Sprint sprint = null;

            startDate = startDate != null ? startDate : DateTime.Now;
            endDate = endDate != null ? endDate : DateTime.Now.AddDays(7);

            if (type.Equals("Release"))
            {
                sprint = new ReleaseSprint((DateTime)startDate, (DateTime)endDate);
            }
            else if (type.Equals("Review"))
            {
                sprint = new ReviewSprint((DateTime)startDate, (DateTime)endDate);
            }

            return sprint;
        }
    }
}
