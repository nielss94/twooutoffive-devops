﻿using System;

namespace TwoOutOfFiveDevOps.Core.Sprints
{
    public class ReleaseSprint : Sprint
    {
        public ReleaseSprint(DateTime startDate, DateTime endDate) : base(startDate, endDate)
        {
        }

        public override void Finish()
        {
			Console.WriteLine("Releasing sprint...");
            base.Finish();
        }
    }
}
