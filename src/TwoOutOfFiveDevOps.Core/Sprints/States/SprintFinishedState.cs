﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public class SprintFinishedState : SprintState
    {
        public SprintFinishedState(Sprint sprint) : base(sprint)
        {
        }
    }
}
