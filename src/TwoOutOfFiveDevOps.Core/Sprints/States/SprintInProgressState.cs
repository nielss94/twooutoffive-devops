﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public class SprintInProgressState : SprintState
    {
        public SprintInProgressState(Sprint sprint) : base(sprint)
        {
        }

        public override void StartPipeline()
        {
            Sprint.SetState(Sprint.SprintPipelineInProgressState);
        }
    }
}
