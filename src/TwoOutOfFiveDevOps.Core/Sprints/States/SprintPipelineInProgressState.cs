﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public class SprintPipelineInProgressState : SprintState
    {
        public SprintPipelineInProgressState(Sprint sprint) : base(sprint)
        {
        }

        public override void Finish()
        {
            Sprint.SetState(Sprint.SprintFinishedState);
        }

        public override void Fail()
        {
            Sprint.SetState(Sprint.SprintFailedState);
        }
    }
}
