﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public abstract class SprintState : IState
    {
        public Sprint Sprint{ get; set; }

        protected SprintState(Sprint sprint)
        {
            Sprint = sprint;
        }

        public virtual void Start()
        {
            Console.WriteLine("Start was not implemented by this state.");
        }

        public virtual void StartPipeline()
        {
            Console.WriteLine("StartPipeline was not implemented by this state.");
        }

        public virtual void Finish()
        {
            Console.WriteLine("Finish was not implemented by this state.");
        }

        public virtual void Fail()
        {
            Console.WriteLine("Fail was not implemented by this state.");
        }
    }
}
