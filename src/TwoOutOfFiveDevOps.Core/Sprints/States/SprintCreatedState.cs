﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public class SprintCreatedState : SprintState
    {
        public SprintCreatedState(Sprint sprint) : base(sprint)
        {
        }

        public override void Start()
        {
            Sprint.SetState(Sprint.SprintInProgressState);
        }
    }
}
