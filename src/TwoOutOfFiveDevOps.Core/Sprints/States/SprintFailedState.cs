﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Sprints.States
{
    //State pattern
    public class SprintFailedState : SprintState
    {
        public SprintFailedState(Sprint sprint) : base(sprint)
        {
        }

        public override void StartPipeline()
        {
            Sprint.SetState(Sprint.SprintPipelineInProgressState);
        }
    }
}
