﻿using System;

namespace TwoOutOfFiveDevOps.Core.Sprints
{
    public class ReviewSprint : Sprint
    {
        public ReviewSprint(DateTime startDate, DateTime endDate) : base(startDate, endDate)
        {
        }

        public override void Finish()
        {
			Console.WriteLine("Reviewing sprint...");
            base.Finish();
        }
    }
}
