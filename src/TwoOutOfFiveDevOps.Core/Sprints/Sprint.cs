﻿using System;
using System.Linq;
using System.Collections.Generic;
using TwoOutOfFiveDevOps.Core.Pipelines;
using TwoOutOfFiveDevOps.Core.Sprints.States;
using TwoOutOfFiveDevOps.Core.Teams;
using TwoOutOfFiveDevOps.Core.Sprints.Reports;
using TwoOutOfFiveDevOps.Core.Observers;
using TwoOutOfFiveDevOps.Core.Backlogs;

namespace TwoOutOfFiveDevOps.Core.Sprints
{
    public abstract class Sprint : ISubject
    {
        private readonly List<Person> team;
        public List<IObserver> Observers { get; private set; }
        public IPipelineTask Pipeline { get; set; }
        public SprintState State { private get; set; }
        public SprintReport SprintReport { get; set; }
        public Backlog Backlog { get; private set; }

        private DateTime startDate;
        public DateTime StartDate {
            get => startDate;
            set
            {
                if(GetState().GetType() != SprintCreatedState.GetType())
                {
                    return;
                }
                startDate = value;
            }
        }

        private DateTime endDate;
        public DateTime EndDate {
            get => endDate;
            set
            {
                if (GetState().GetType() != SprintCreatedState.GetType())
                {
                    return;
                }
                endDate = value;
            }
        }

        protected Sprint(DateTime startDate, DateTime endDate)
        {
            State = SprintCreatedState;

            StartDate = startDate;
            EndDate = endDate;

            team = new List<Person>();
            Observers = new List<IObserver>();
            Backlog = new Backlog();
        }

        public void AddToBacklog(BacklogTask backlogTask)
        {
            if (GetState().GetType() != SprintCreatedState.GetType())
            {
                return;
            }
            Backlog.AddBacklogTask(backlogTask);
        }

        public void AddToTeam(Person person)
        {
            if(person is ScrumMaster && HasScrumMaster())
            {
                throw new TeamsCanOnlyHaveOneScrumMasterException("Teams can only have one Scrum Master.");
            }

            if (person is ProductOwner)
            {
                throw new TeamsCanOnlyContainDevelopersAndScrumMastersException("Teams can only contain developers and Scrum Masters.");
            }

            team.Add(person);
        }

        public void AddToTeam(List<Person> people)
        {
            foreach(var person in people)
            {
                AddToTeam(person);
            }
        }

        public void SetState(SprintState state)
        {
            State = state;
            NotifyObservers();
        }
        
        public void Start()
        {
            State.Start();
        }

        public void StartPipeline()
        {
            State.StartPipeline();
            if (Pipeline.Execute())
            {
                Finish();
            }
            else
            {
                Fail();
            }
        }

        public virtual void Finish()
        {
            State.Finish();
        }

        public void Fail()
        {
            State.Fail();
        }

        public SprintState SprintCreatedState => new SprintCreatedState(this);
        public SprintState SprintInProgressState => new SprintInProgressState(this);
        public SprintState SprintPipelineInProgressState => new SprintPipelineInProgressState(this);
        public SprintState SprintFailedState => new SprintFailedState(this);
        public SprintState SprintFinishedState => new SprintFinishedState(this);

        private bool HasScrumMaster()
        {
            return team.OfType<ScrumMaster>().Any();
        }

        public void NotifyObservers()
        {
            foreach (var observer in Observers)
            {
                observer.Update();
            }
        }

        public void RegisterObserver(IObserver observer)
        {
            Observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            Observers.Remove(observer);
        }

        public IState GetState()
        {
            return State;
        }
    }
}
