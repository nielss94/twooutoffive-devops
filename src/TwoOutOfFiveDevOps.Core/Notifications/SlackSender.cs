using System;

namespace TwoOutOfFiveDevOps.Core.Notifications
{
    //Decorator pattern implemented
    public class SlackSender: SenderAddition
    {
        public SlackSender(ISender sender, string destination) : base(sender, destination)
        {
        }

        public override void Send(string message)
        {
            Sender.Send(message);
            Console.WriteLine("Sending slack to " + Destination + ": " + message);
        }
    }
}