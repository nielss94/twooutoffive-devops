using System;

namespace TwoOutOfFiveDevOps.Core.Notifications
{
    //Decorator pattern implemented
    public class EmailSender: SenderAddition
    {
        public EmailSender(ISender sender, string destination) : base(sender, destination)
        {
        }

        public override void Send(string message)
        {
            Sender.Send(message);
            Console.WriteLine("Sending email to " + Destination + ": " + message);
        }
    }
}