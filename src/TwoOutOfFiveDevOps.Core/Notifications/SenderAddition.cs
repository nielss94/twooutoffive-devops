using System;

namespace TwoOutOfFiveDevOps.Core.Notifications
{
    //Decorator pattern implemented
    public abstract class SenderAddition: ISender
    {
        protected ISender Sender { get; }
        protected string Destination { get; }

        protected SenderAddition(ISender sender, string destination)
        {
            Sender = sender;
            Destination = destination;
        }

        
        public abstract void Send(string message);
    }
}