namespace TwoOutOfFiveDevOps.Core.Notifications
{
    //Decorator pattern implemented
    public interface ISender
    {
        void Send(string message);
    }
}