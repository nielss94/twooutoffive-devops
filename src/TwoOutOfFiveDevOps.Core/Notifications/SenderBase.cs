using System;

namespace TwoOutOfFiveDevOps.Core.Notifications
{
    //Decorator pattern implemented
    public class SenderBase: ISender
    {
        public void Send(string message)
        {
            Console.WriteLine("Starting to send messages...");
        }
    }
}