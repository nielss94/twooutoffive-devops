﻿using System;

namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public class PipelineTaskBase : IPipelineTask
    {
        public bool Execute()
        {
            Console.WriteLine("Collecting source code... Success!");
            Console.WriteLine("Installing packages... Success!");
            Console.WriteLine("Building project... Success!");
            
            return true;
        }
    }
}
