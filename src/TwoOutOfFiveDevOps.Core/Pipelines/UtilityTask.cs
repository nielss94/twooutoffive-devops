﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public class UtilityTask : PipelineTaskAddition
    {
        public UtilityTask(IPipelineTask pipelineTask) : base(pipelineTask)
        {
        }

        public override bool Execute()
        {
            if (!PipelineTask.Execute())
                return false;

            Console.WriteLine("Updating all family and friends of our wonderful accomplishment... Success!");
            return true;
        }
    }
}
