﻿using System;

namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public class DeployTask : PipelineTaskAddition
    {
        public DeployTask(IPipelineTask pipelineTask) : base(pipelineTask)
        {
        }

        public override bool Execute()
        {
            if (!PipelineTask.Execute())
                return false;

            Console.WriteLine("Deploying... Success!");
            return true;
        }
    }
}
