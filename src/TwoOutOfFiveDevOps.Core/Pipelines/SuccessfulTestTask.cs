﻿using System;

namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public class SuccessfulTestTask : PipelineTaskAddition
    {
        public SuccessfulTestTask(IPipelineTask pipelineTask) : base(pipelineTask)
        {
        }

        public override bool Execute()
        {
            if (!PipelineTask.Execute())
                return false;

            Console.WriteLine("Running test task... Success!");
            return true;
        }
    }
}
