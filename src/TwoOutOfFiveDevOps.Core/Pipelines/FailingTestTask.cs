﻿using System;

namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public class FailingTestTask : PipelineTaskAddition
    {
        public FailingTestTask(IPipelineTask pipelineTask) : base(pipelineTask)
        {
        }

        public override bool Execute()
        {
            if (!PipelineTask.Execute())
                return false;

            Console.WriteLine("Running test task... Failed!");
            return false;
        }
    }
}
