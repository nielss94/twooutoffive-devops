﻿namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public interface IPipelineTask
    {
        bool Execute();
    }
}
