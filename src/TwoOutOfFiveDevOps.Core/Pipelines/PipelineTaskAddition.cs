﻿namespace TwoOutOfFiveDevOps.Core.Pipelines
{
    //Decorator pattern
    public abstract class PipelineTaskAddition : IPipelineTask
    {
        public IPipelineTask PipelineTask { get; }

        protected PipelineTaskAddition(IPipelineTask pipelineTask)
        {
            PipelineTask = pipelineTask;
        }

        public abstract bool Execute();
    }
}
