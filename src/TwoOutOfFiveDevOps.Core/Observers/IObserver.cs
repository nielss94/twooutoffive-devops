namespace TwoOutOfFiveDevOps.Core.Observers
{
    // Observer pattern implemented
    public interface IObserver
    {
        void Update(IState state = null);
    }
}