namespace TwoOutOfFiveDevOps.Core.Observers
{
    // Observer pattern implemented
    public interface ISubject
    {
        void NotifyObservers();
        void RegisterObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        IState GetState();
    }
}