using System;

namespace TwoOutOfFiveDevOps.Core.SCM
{
    //Strategy pattern implemented
    public class SvnManager: IScmManager
    {
        public void Merge()
        {
            Console.WriteLine("SVN: Merging");
        }

        public void Branch()
        {
            Console.WriteLine("SVN: Branching");
        }
    }
}
