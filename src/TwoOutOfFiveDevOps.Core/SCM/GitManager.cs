using System;

namespace TwoOutOfFiveDevOps.Core.SCM
{
    //Strategy pattern implemented
    public class GitManager: IScmManager
    {
        public void Merge()
        {
            Console.WriteLine("Git: Merging");
        }

        public void Branch()
        {
            Console.WriteLine("Git: Branching");
        }
    }
}
