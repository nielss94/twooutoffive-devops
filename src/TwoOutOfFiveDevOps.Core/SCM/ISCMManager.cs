namespace TwoOutOfFiveDevOps.Core.SCM
{
    //Strategy pattern implemented
    public interface IScmManager
    {
        void Merge();
        void Branch();
    }
}
