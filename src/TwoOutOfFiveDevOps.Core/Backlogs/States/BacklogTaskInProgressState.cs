namespace TwoOutOfFiveDevOps.Core.Backlogs.States
{
    //State pattern implemented
    public class BacklogTaskInProgressState: BacklogTaskState
    {
        public BacklogTaskInProgressState(BacklogTask backlogTask) : base(backlogTask)
        {
        }

        public override void Finish()
        {
            BacklogTask.SetState(BacklogTask.GetBacklogTaskFinishedState());
        }
    }
}