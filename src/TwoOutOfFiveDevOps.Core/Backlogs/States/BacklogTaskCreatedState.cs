namespace TwoOutOfFiveDevOps.Core.Backlogs.States
{
    //State pattern implemented
    public class BacklogTaskCreatedState: BacklogTaskState
    {
        public BacklogTaskCreatedState(BacklogTask backlogTask) : base(backlogTask)
        {
        }

        public override void Start()
        {
            BacklogTask.SetState(BacklogTask.GetBacklogTaskInProgressState());
        }
    }
}