namespace TwoOutOfFiveDevOps.Core.Backlogs.States
{
    //State pattern implemented
    public class BacklogFinishedState : BacklogTaskState
    {
        public BacklogFinishedState(BacklogTask backlogTask) : base(backlogTask)
        {
        }

        public override void Restart()
        {
            BacklogTask.SetState(BacklogTask.GetBacklogTaskCreatedState());
        }
    }
}