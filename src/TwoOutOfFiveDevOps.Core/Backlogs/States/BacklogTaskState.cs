using System;

namespace TwoOutOfFiveDevOps.Core.Backlogs.States
{
    //State pattern implemented
    public abstract class BacklogTaskState: IState
    {
        protected BacklogTask BacklogTask { get; set; }

        protected BacklogTaskState(BacklogTask backlogTask)
        {
            BacklogTask = backlogTask;
        }

        public virtual void Start()
        {
            Console.WriteLine("Start is not implemented by this state");
        }

        public virtual void Finish()
        {
            Console.WriteLine("Finish is not implemented by this state");
        }

        public virtual void Restart()
        {
            Console.WriteLine("Restart is not implemented by this state");
        }
    }
}