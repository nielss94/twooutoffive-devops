using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.SCM;

namespace TwoOutOfFiveDevOps.Core.Backlogs
{
    public class Backlog
    {
        private readonly List<BacklogTask> backlogTasks;

        public Backlog()
        {
            backlogTasks = new List<BacklogTask>();
        }

        public void AddBacklogTask(BacklogTask backlogTask, IScmManager scmManager = null)
        {
            backlogTask.ScmManager = scmManager;
            backlogTasks.Add(backlogTask);
        }

        public void RemoveBacklogTask(BacklogTask backlogTask)
        {
            backlogTasks.Remove(backlogTask);
        }

        public BacklogTask GetBacklogTask(int index)
        {
            return backlogTasks.ElementAtOrDefault(index);
        }

        public List<BacklogTask> GetBacklogTasks()
        {
            return backlogTasks;
        }
    }
}