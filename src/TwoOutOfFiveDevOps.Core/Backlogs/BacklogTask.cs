using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.Backlogs.States;
using TwoOutOfFiveDevOps.Core.Observers;
using TwoOutOfFiveDevOps.Core.SCM;
using TwoOutOfFiveDevOps.Core.Teams;

namespace TwoOutOfFiveDevOps.Core.Backlogs
{
    //State pattern and Observer pattern implemented
    public class BacklogTask: ISubject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Developer Developer { get; private set; }
        public IScmManager ScmManager { private get; set; }
        
        private BacklogTaskState state;
        private readonly List<BacklogTask> children;
        private readonly List<IObserver> observers;

        public BacklogTask()
        {
            children = new List<BacklogTask>();
            observers = new List<IObserver>();
            state = GetBacklogTaskCreatedState();
        }

        public BacklogTask(string name, string description, IScmManager scmManager): this()
        {
            Name = name;
            Description = description;
            ScmManager = scmManager;
        }

        public void AddChild(BacklogTask task)
        {
            children.Add(task);
        }

        public void RemoveChild(BacklogTask task)
        {
            children.Remove(task);
        }

        public BacklogTask GetChild(int index)
        {
            return children.ElementAtOrDefault(index);
        }

        public void AssignDeveloper(Person developer)
        {
            if(developer.GetType() == typeof(Developer))
            {
                Developer = (Developer) developer;
            }
            else
            {
                throw new MustBeADeveloperException("The type must be a developer");
            }
        }

        private bool CanFinish()
        {
            bool canFinish = true;

            for (int i = 0; i < children.Count ; i++)
            {
                BacklogTask task = GetChild(i);
                canFinish = task.state.GetType() == typeof(BacklogFinishedState);
                if (canFinish)
                {
                    canFinish = task.CanFinish();
                }
            }
            
            return canFinish;
        }

        public void Start()
        {
            state.Start();
        }

        public void Finish()
        {
            if (CanFinish())
            {
                state.Finish();
            }
        }

        public void Restart()
        {
            state.Restart();
        }

        public void SetState(BacklogTaskState state)
        {
            this.state = state;
            NotifyObservers();
        }

        public BacklogTaskState GetBacklogTaskCreatedState()
        {
            return new BacklogTaskCreatedState(this);
        }
        
        public BacklogTaskState GetBacklogTaskInProgressState()
        {
            return new BacklogTaskInProgressState(this);
        }
        
        public BacklogTaskState GetBacklogTaskFinishedState()
        {
            return new BacklogFinishedState(this);
        }

        public void NotifyObservers()
        {
            foreach (IObserver observer in observers)
            {
                observer.Update();
            }
        }

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public IState GetState()
        {
            return state;
        }

        public void Branch()
        {
            ScmManager?.Branch();
        }
    }
}