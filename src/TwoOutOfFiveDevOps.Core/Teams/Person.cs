﻿using TwoOutOfFiveDevOps.Core.Notifications;
using TwoOutOfFiveDevOps.Core.Observers;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    public abstract class Person : IObserver
    {
        public string Name { get; }
        public ISender Sender { get; private set; }
        
        protected Person(string name)
        {
            Name = name;
        }

        public void AddWhatsAppNotification(string phoneNumber)
        {
            Sender = new WhatsAppSender(Sender, phoneNumber);
        }
        
        public void AddEmailNotification(string email)
        {
            Sender = new EmailSender(Sender, email);
        }

        public void AddSlackNotification(string slackUsername)
        {
            Sender = new SlackSender(Sender, slackUsername);
        }

        public abstract void Update(IState state = null);
        
    }
}
