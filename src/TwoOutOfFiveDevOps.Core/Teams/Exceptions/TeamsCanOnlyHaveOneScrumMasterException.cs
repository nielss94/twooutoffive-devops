﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    [Serializable]
    public class TeamsCanOnlyHaveOneScrumMasterException : Exception
    {
        public TeamsCanOnlyHaveOneScrumMasterException(string message) : base(message)
        {
        }
    }
}
