using System;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    [Serializable]
    public class MustBeADeveloperException: Exception
    {
        public MustBeADeveloperException(string message): base(message) {}
    }
}