﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    [Serializable]
    public class TeamsCanOnlyContainDevelopersAndScrumMastersException : Exception
    {
        public TeamsCanOnlyContainDevelopersAndScrumMastersException(string message) : base(message)
        {
        }
    }
}
