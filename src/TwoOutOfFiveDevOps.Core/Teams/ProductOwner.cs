﻿using TwoOutOfFiveDevOps.Core.Sprints.States;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    public class ProductOwner : Person
    {
        public ProductOwner(string name) : base(name)
        {
        }

        public override void Update(IState state = null)
        {
            if ((SprintState)state is SprintFinishedState)
            {
                Sender.Send("Sprint finished!");
            }
        }
    }
}
