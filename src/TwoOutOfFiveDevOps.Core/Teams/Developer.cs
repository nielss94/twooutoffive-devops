﻿using System;

namespace TwoOutOfFiveDevOps.Core.Teams
{
    public class Developer : Person
    {
        public Developer(string name) : base(name)
        {
        }

        public override void Update(IState state = null)
        {
            Console.WriteLine("Update developer...");
        }
    }
}
