using TwoOutOfFiveDevOps.Core.SCM;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.SCM
{
    public class GitManagerTest
    {
        private readonly IScmManager gitManager = new GitManager();

        [Fact]
        public void GitCanMerge()
        {
            gitManager.Merge();
        }

        [Fact]
        public void GitCanBranch()
        {
            gitManager.Branch();
        }
    }
}
