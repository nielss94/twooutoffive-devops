using TwoOutOfFiveDevOps.Core.SCM;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.SCM
{
    public class SVNManagerTest
    {
        private readonly IScmManager svnManager = new SvnManager();

        [Fact]
        public void SVNCanMerge()
        {
            svnManager.Merge();
        }

        [Fact]
        public void SVNCanBranch()
        {
            svnManager.Branch();
        }
    }
}
