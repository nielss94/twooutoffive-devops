using System.Collections.Generic;
using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.Discussions;
using TwoOutOfFiveDevOps.Core.Teams;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Discussions
{
    public class ForumTest
    {
        private readonly Thread thread = new Thread.Builder(new BacklogTask())
            .WithTitle("Test thread")
            .WithSubject("test")
            .WithOwner(new Developer("Bart"))
            .Build();
        
        [Fact]
        public void CanAddThreadToAForum()
        {
            Forum forum = new Forum();
            forum.AddThread(thread);
            
            Assert.Equal(thread, forum.GetThread(0));
        }

        [Fact]
        public void CanGetThreadsFromAForum()
        {
            Forum forum = new Forum();
            forum.AddThread(thread);
            
            Assert.Single(forum.GetThreads());
        }
        
        [Fact]
        public void CanRemoveAThreadFromAForum()
        {
            Forum forum = new Forum();
            
            forum.AddThread(thread);
            Assert.Equal(thread, forum.GetThread(0));
            
            forum.RemoveThread(thread);
            Assert.Null(forum.GetThread(0));
        }

        [Fact]
        public void CanGetAllThreadsFromAForum()
        {
            Forum forum = new Forum();          
            forum.AddThread(thread);
            
            Assert.IsType<List<Thread>>(forum.GetThreads());
        }
    }
}
