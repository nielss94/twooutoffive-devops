﻿using Xunit;
using TwoOutOfFiveDevOps.Core.Discussions;
using TwoOutOfFiveDevOps.Core.Teams;

namespace TwoOutOfFiveDevOps.Test.Discussions
{
    public class CommentTest
    {
        private readonly Comment comment = new Comment("I agree", new Developer("Bart"));

        [Fact]
        public void CommentHasContent()
        {
            Assert.Equal("I agree", comment.Content);
        }

        [Fact]
        public void CommentHasAOwner()
        {
            Assert.Equal("Bart", comment.Owner.Name);
        }

        [Fact]
        public void CommentHasAComment()
        {
            comment.AddChild(new Comment("I do not", new ScrumMaster("Marc")));

            Assert.Equal("I do not", comment.GetChild(0).Content);
            Assert.Equal("Marc", comment.GetChild(0).Owner.Name);
        }

        [Fact]
        public void CommentCanBeRemoved()
        {
            comment.AddChild(new Comment("I do not", new ScrumMaster("Marc")));

            Comment marcComment = comment.GetChild(0);
            comment.RemoveChild(marcComment);

            Assert.Equal(0, comment.GetNumberOfComments());
        }

        [Fact] 
        public void CommentOfACommentCanHaveAComment()
        {
            comment.AddChild(new Comment("I do not", new ScrumMaster("Marc")));

            Comment marcComment = comment.GetChild(0);
            marcComment.AddChild(new Comment("Marc!", new Developer("Niels")));

            Comment nielsComment = comment.GetChild(0).GetChild(0);

            Assert.Equal("Marc!", nielsComment.Content);
            Assert.Equal("Niels", nielsComment.Owner.Name);
        }
    }
}
