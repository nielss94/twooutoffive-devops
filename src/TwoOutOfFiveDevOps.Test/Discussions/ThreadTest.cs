using System.Collections.Generic;
using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.Discussions;
using TwoOutOfFiveDevOps.Core.Teams;
using Xunit;
using Thread = TwoOutOfFiveDevOps.Core.Discussions.Thread;

namespace TwoOutOfFiveDevOps.Test.Discussions
{
    public class ThreadTest
    {
        
        private readonly BacklogTask backlogTask = new BacklogTask();
        
        [Fact]
        public void CanCreateAThreadWithASubject()
        {
            const string subject = "Bug";
            Thread thread = new Thread.Builder(backlogTask)
                .WithSubject(subject)
                .Build();
            
            Assert.Equal(subject, thread.Subject);
        }
        
        [Fact]
        public void CanCreateAThreadWithAOwner()
        {
            const string name = "Bart";
            Person person = new Developer(name);
            Thread thread = new Thread.Builder(backlogTask)
                .WithOwner(person)
                .Build();
            
            Assert.Equal(name, thread.Owner.Name);
        }
        
        [Fact]
        public void CanCreateAThreadWithATitle()
        {
            const string title = "This is epic!";
            Thread thread = new Thread.Builder(backlogTask)
                .WithTitle(title)
                .Build();
            
            Assert.Equal(title, thread.Title);
        }
        
        [Fact]
        public void CanCreateAThreadWithContent()
        {
            const string content = "We should be doing this!";
            Thread thread = new Thread.Builder(backlogTask)
                .WithContent(content)
                .Build();
            
            Assert.Equal(content, thread.Content);
        }
        
        [Fact]
        public void CanCreateAThreadWithAComment()
        {
            Comment comment = new Comment("Wow", new Developer("Bart"));
            
            Thread thread = new Thread.Builder(backlogTask)
                .WithComment(comment)
                .Build();
            
            Assert.Equal(comment, thread.GetComment(0));
        }
        
        [Fact]
        public void CanCreateAThreadWithComments()
        {
            Comment comment1 = new Comment("Cool", new Developer("Bart"));
            Comment comment2 = new Comment("Epic!", new Developer("Niels"));
            
            IEnumerable<Comment> comments = new List<Comment>(2) { comment1, comment2 };
            
            Thread thread = new Thread.Builder(backlogTask)
                .WithComments(comments)
                .Build();
            
            Assert.Equal(comment1, thread.GetComment(0));
            Assert.Equal(comment2, thread.GetComment(1));
        }
        
        [Fact]
        public void CanAddACommentToAThread()
        {
            Comment comment = new Comment("What is this?", new Developer("Bart"));
            
            Thread thread = new Thread.Builder(backlogTask).Build();
            thread.AddComment(comment);
            
            Assert.Equal(comment, thread.GetComment(0));
        }
        
        [Fact]
        public void CanRemoveACommentFromAThread()
        {
            Comment comment = new Comment("I never make a tiepo", new Developer("Bart"));
            
            Thread thread = new Thread.Builder(backlogTask)
                .WithComment(comment)
                .Build();
           
            thread.RemoveComment(comment);
            
            Assert.Null(thread.GetComment(0));
        }

        [Fact]
        public void CanNotCommentOnAClosedThread()
        {
            Comment comment = new Comment("Nice", new Developer("Bart"));
            
            BacklogTask backlogTask = new BacklogTask();
            Thread thread = new Thread.Builder(backlogTask)
                .Build();
            
            backlogTask.Start();
            backlogTask.Finish();
            
            thread.AddComment(comment);
            Assert.Null(thread.GetComment(0));
        }
        
        [Fact]
        public void CanCommentOnARestartedBacklogTaskThatOpensThread()
        {
            Comment comment = new Comment("Nice", new Developer("Bart"));
            
            BacklogTask backlogTask = new BacklogTask();
            Thread thread = new Thread.Builder(backlogTask)
                .Build();
            
            backlogTask.Start();
            backlogTask.Finish();
            
            thread.AddComment(comment);
            Assert.Null(thread.GetComment(0));
            
            backlogTask.Restart();
            thread.AddComment(comment);
            Assert.Equal(comment, thread.GetComment(0));
        }
        
        [Fact]
        public void CanNotCommentOnACommentOfAClosedThread()
        {
            Comment comment = new Comment("Nice", new Developer("Bart"));
            Comment comment2 = new Comment("Yes", new Developer("Niels"));
            
            BacklogTask backlogTask = new BacklogTask();
            Thread thread = new Thread.Builder(backlogTask)
                .Build();
            
            backlogTask.Start();
            
            thread.AddComment(comment);
            Assert.Equal(comment, thread.GetComment(0));
            
            backlogTask.Finish();
            comment.AddChild(comment2);
            
            Assert.Null(comment.GetChild(0));
        }
        
        [Fact]
        public void CanCommentOnACommentOfANonClosedThread()
        {
            Comment comment = new Comment("Nice", new Developer("Bart"));
            Comment comment2 = new Comment("Yes", new Developer("Niels"));
            
            BacklogTask backlogTask = new BacklogTask();
            Thread thread = new Thread.Builder(backlogTask)
                .Build();
            
            backlogTask.Start();
            
            thread.AddComment(comment);
            Assert.Equal(comment, thread.GetComment(0));
            
            comment.AddChild(comment2);
            
            Assert.Equal(comment2, comment.GetChild(0));
        }
    }
}
