﻿using System;
using System.Collections.Generic;
using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.Pipelines;
using TwoOutOfFiveDevOps.Core.Sprints;
using TwoOutOfFiveDevOps.Core.Sprints.Factory;
using TwoOutOfFiveDevOps.Core.Sprints.Reports;
using TwoOutOfFiveDevOps.Core.Sprints.States;
using TwoOutOfFiveDevOps.Core.Teams;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Sprints
{
    public class SprintTest
    {
        private Sprint sprint;
        private readonly ISprintFactory sprintFactory = new ConcreteSprintFactory();
        private readonly List<Person> team = new List<Person>();

        [Fact]
        public void SprintCanBeReleaseSprint()
        {
            sprint = sprintFactory.CreateSprint("Release", null, null);

            Assert.IsType<ReleaseSprint>(sprint);
        }

        [Fact]
        public void SprintCanBeReviewSprint()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            Assert.IsType<ReviewSprint>(sprint);
        }

        [Fact]
        public void SprintCanFail()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            IPipelineTask pipeline = new PipelineTaskBase();
            sprint.Pipeline = new DeployTask(new FailingTestTask(pipeline));

            sprint.Start();
            sprint.StartPipeline();
            
            Assert.IsType<SprintFailedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCanRestartPipelineOnFail()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            IPipelineTask pipeline = new PipelineTaskBase();
            sprint.Pipeline = new DeployTask(new FailingTestTask(pipeline));

            sprint.Start();
            sprint.StartPipeline();
            sprint.StartPipeline();

            Assert.IsType<SprintFailedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCannotStartFromInProgressState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);
            
            sprint.Start();
            sprint.Start();

            Assert.IsType<SprintInProgressState>(sprint.GetState());
        }

        [Fact]
        public void SprintCannotStartPipelineFromCreatedState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            IPipelineTask pipeline = new PipelineTaskBase();
            sprint.Pipeline = new DeployTask(new SuccessfulTestTask(pipeline));

            sprint.Start();
            sprint.StartPipeline();
            sprint.StartPipeline();

            Assert.IsType<SprintFinishedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCannotFinishFromCreatedState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.Finish();

            Assert.IsType<SprintCreatedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCannotFailFromCreatedState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.Fail();

            Assert.IsType<SprintCreatedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCanFinishAsRelease()
        {
            sprint = sprintFactory.CreateSprint("Release", null, null);

            IPipelineTask pipeline = new PipelineTaskBase();
            sprint.Pipeline = new DeployTask(new SuccessfulTestTask(pipeline));

            sprint.Start();
            sprint.StartPipeline();

            Assert.IsType<SprintFinishedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCanFinishAsReview()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            IPipelineTask pipeline = new PipelineTaskBase();
            sprint.Pipeline = new DeployTask(new SuccessfulTestTask(pipeline));

            sprint.Start();
            sprint.StartPipeline();

            Assert.IsType<SprintFinishedState>(sprint.GetState());
        }

        [Fact]
        public void SprintCanGeneratePDFReport()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.SprintReport = new PdfSprintReport(sprint);
            sprint.SprintReport.GenerateReport();

            Assert.IsType<PdfSprintReport>(sprint.SprintReport);
        }

        [Fact]
        public void SprintCanGeneratePNGReport()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.SprintReport = new PngSprintReport(sprint);
            sprint.SprintReport.GenerateReport();

            Assert.IsType<PngSprintReport>(sprint.SprintReport);
        }
        

        [Fact]
        public void SprintHasBeginAndEndDate()
        {
            sprint = sprintFactory.CreateSprint("Review",null,null);

            Assert.Equal(DateTime.Now.Date,sprint.StartDate.Date);
            Assert.Equal(DateTime.Now.AddDays(7).Date, sprint.EndDate.Date);
        }

        [Fact]
        public void SprintCanOnlyHaveOneScrumMaster()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            Person marc = new ScrumMaster("Marc");
            Person bart = new Developer("Bart");
            Person rick = new ScrumMaster("Rick");
            
            void Action() => sprint.AddToTeam(new List<Person>
            {
                marc,
                bart,
                rick
            });
            
            Assert.Throws<TeamsCanOnlyHaveOneScrumMasterException>((Action)Action);
        }

        [Fact]
        public void SprintCanOnlyContainScrumMasterAndDevelopers()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            Person marc = new ScrumMaster("Marc");
            Person bart = new Developer("Bart");
            Person ruud = new ProductOwner("Ruud");

            void Action() => sprint.AddToTeam(new List<Person>
            {
                marc,
                bart,
                ruud
            });

            Assert.Throws<TeamsCanOnlyContainDevelopersAndScrumMastersException>((Action)Action);
        }

        [Fact]
        public void SprintCanAddToBacklog()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.AddToBacklog(new BacklogTask());
            Assert.Single(sprint.Backlog.GetBacklogTasks());
        }

        [Fact]
        public void SprintCannotAddToBacklogOutsideOfCreatedState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.Start();

            sprint.AddToBacklog(new BacklogTask());
            Assert.Empty(sprint.Backlog.GetBacklogTasks());
        }

        [Fact]
        public void SprintCannotEditDatesOutsideOfCreatedState()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            sprint.Start();

            sprint.StartDate = new DateTime();
            sprint.EndDate = new DateTime();

            Assert.Equal(DateTime.Now.Date, sprint.StartDate.Date);
            Assert.Equal(DateTime.Now.Date.AddDays(7), sprint.EndDate.Date);
        }

        [Fact]
        public void SprintRegisterAndRemovesObservers()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            Person scrumMaster = new ScrumMaster("Marc");
            sprint.RegisterObserver(scrumMaster);

            Assert.Single(sprint.Observers);

            sprint.RemoveObserver(scrumMaster);

            Assert.Empty(sprint.Observers);
        }

        [Fact]
        public void SprintCanHaveMultipleObservers()
        {
            sprint = sprintFactory.CreateSprint("Review", null, null);

            Person scrumMaster = new ScrumMaster("Marc");
            Person developer = new ScrumMaster("Bart");
            Person productOwner = new ProductOwner("Ruud");

            sprint.RegisterObserver(scrumMaster);
            sprint.RegisterObserver(developer);
            sprint.RegisterObserver(productOwner);

            sprint.Start();
            
            Assert.Equal(3,sprint.Observers.Count);
        }
    }
}
