﻿using Xunit;
using TwoOutOfFiveDevOps.Core.Pipelines;

namespace TwoOutOfFiveDevOps.Test.Pipelines
{
    public class PipelineTest
    {
        private IPipelineTask pipeline = new PipelineTaskBase();

        [Fact]
        public void PipelineCanFail()
        {
            pipeline = new FailingTestTask(new SuccessfulTestTask(pipeline));

            Assert.False(pipeline.Execute());
        }

        [Fact]
        public void PipelineCanHaveMultipleTasks()
        {
            pipeline = new SuccessfulTestTask(new DeployTask(new UtilityTask(pipeline)));

            Assert.True(pipeline.Execute());
        }

        [Fact]
        public void PipelineCanHaveOnlyBase()
        {
            Assert.True(pipeline.Execute());
        }
    }
}
