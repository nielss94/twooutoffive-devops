using Xunit;
using TwoOutOfFiveDevOps.Core;

namespace TwoOutOfFiveDevOps.Test
{
    public class ProgramTest
    {
        [Fact]
        public void ProgramCanRun()
        {
            Program.Main(new string[0]);
        }
    }
}
