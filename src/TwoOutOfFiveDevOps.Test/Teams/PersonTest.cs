using Xunit;
using TwoOutOfFiveDevOps.Core.Teams;

namespace TwoOutOfFiveDevOps.Test.Teams
{
    public class PersonTest
    {
        [Fact]
        public void PersonHasAName()
        {
            const string name = "test";
            Person productOwner = new ProductOwner(name);
            Person developer = new Developer(name);
            Person scrumMaster = new ScrumMaster(name);

            Assert.Equal(name, productOwner.Name);
            Assert.Equal(name, developer.Name);
            Assert.Equal(name, scrumMaster.Name);
        }

        [Fact]
        public void PersonCanHaveEmailNotifications()
        {
            Person developer = new Developer("Bart");
            developer.AddEmailNotification("mail@gmail.com");
        }
        
        [Fact]
        public void PersonCanHaveWhatsAppNotifications()
        {
            Person developer = new Developer("Bart");
            developer.AddWhatsAppNotification("0612345678");
        }
        
        [Fact]
        public void PersonCanHaveSlackNotifications()
        {
            Person developer = new Developer("Bart");
            developer.AddSlackNotification("slackusername");
        }
    }
}
