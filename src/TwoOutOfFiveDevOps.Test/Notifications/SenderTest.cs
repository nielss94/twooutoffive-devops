using TwoOutOfFiveDevOps.Core.Notifications;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Notifications
{
    public class SenderTest
    {
        private const string testMessage = "Test message";

        [Fact]
        public void CanSendMessageWithBase()
        {
            ISender sender = new SenderBase();
            sender.Send(testMessage);
        }

        [Fact]
        public void CanSendMessageToWhatsApp()
        {
            ISender sender = new SenderBase();
            sender = new WhatsAppSender(sender, "0612345678");
            sender.Send(testMessage);
        }

        [Fact]
        public void CanSendMessageToEmail()
        {
            ISender sender = new SenderBase();
            sender = new EmailSender(sender, "email@gmail.com");
            sender.Send(testMessage);
        }
        
        [Fact]
        public void CanSendMessageToSlack()
        {
            ISender sender = new SenderBase();
            sender = new SlackSender(sender, "slackusername");
            sender.Send(testMessage);
        }
        
        [Fact]
        public void CanSendMessageWithMultipleSenders()
        {
            ISender sender = new SenderBase();
            sender = new SlackSender(sender, "slackusername");
            sender = new EmailSender(sender, "email@gmail.com");
            sender.Send(testMessage);
        }
    }
}