using System;
using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.SCM;
using TwoOutOfFiveDevOps.Core.Teams;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Backlogs
{
    public class BacklogTaskTest
    {
        [Fact]
        public void BacklogTaskCanHaveABacklogTask()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask backlogTaskToAdd = new BacklogTask();
            backlogTask.AddChild(backlogTaskToAdd);
            
            Assert.Equal(backlogTaskToAdd, backlogTask.GetChild(0));
        }
        
        [Fact]
        public void BacklogTaskCanRemoveABacklogTask()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask backlogTaskToAdd = new BacklogTask();
            backlogTask.AddChild(backlogTaskToAdd);
            backlogTask.RemoveChild(backlogTaskToAdd);
            
            Assert.Null(backlogTask.GetChild(0));
        }

        [Fact]
        public void BacklogTaskOfABacklogTaskCanHaveABacklogTask()
        {
            
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask backlogTaskOfTheFirstOne = new BacklogTask();        
            BacklogTask backlogTaskToAdd = new BacklogTask();
            
            backlogTaskOfTheFirstOne.AddChild(backlogTaskToAdd);
            backlogTask.AddChild(backlogTaskOfTheFirstOne);
            
            Assert.Equal(backlogTaskToAdd, backlogTask.GetChild(0).GetChild(0));
        }

        [Fact]
        public void CanCreateABacklogTaskWithNameAndDescription()
        {
            const string name = "Do something";
            const string description = "Do it this way";
            
            BacklogTask backlogTask = new BacklogTask(name, description, new GitManager());
            
            Assert.Equal(name, backlogTask.Name);
            Assert.Equal(description, backlogTask.Description);
        }
        
        [Fact]
        public void CanEditABacklogTasksNameAndDescription()
        {
            const string name = "Do something";
            const string description = "Do it this way";

            const string newName = "No do this";
            const string newDescription = "Now do it this way";
            
            BacklogTask backlogTask =  new BacklogTask(name, description, new GitManager());
            
            backlogTask.Name = newName;
            backlogTask.Description = newDescription;
            
            Assert.Equal(newName, backlogTask.Name);
            Assert.Equal(newDescription, backlogTask.Description);
        }

        [Fact]
        public void CanCreateABacklogTaskThatCanBranch()
        {
            const string name = "Do something";
            const string description = "Do it this way";
            
            BacklogTask backlogTask =  new BacklogTask(name, description, new GitManager());
            
            Assert.Equal(name, backlogTask.Name);
            Assert.Equal(description, backlogTask.Description);
            
            backlogTask.Branch();
        }
        
        [Fact]
        public void CanAssignADeveloperToBacklogTask()
        {
            BacklogTask backlogTask = new BacklogTask();
            Person developer = new Developer("Bart");
            
            backlogTask.AssignDeveloper(developer);
            
            Assert.Equal(developer, backlogTask.Developer);
        }
        
        [Fact]
        public void CanNotAssignAScrumMasterToBacklogTask()
        {
            BacklogTask backlogTask = new BacklogTask();
            Person scrumMaster = new ScrumMaster("Marc");

            void Action() => backlogTask.AssignDeveloper(scrumMaster);
            Assert.Throws<MustBeADeveloperException>((Action) Action);
        }
        
        [Fact]
        public void CanNotAssignAProductOwnerToBacklogTask()
        {
            BacklogTask backlogTask = new BacklogTask();
            Person productOwner = new ProductOwner("Ruud");

            void Action() => backlogTask.AssignDeveloper(productOwner);
            Assert.Throws<MustBeADeveloperException>((Action) Action);
        }
        
    }
}
