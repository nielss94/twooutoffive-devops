using System.Collections.Generic;
using System.Linq;
using TwoOutOfFiveDevOps.Core.Backlogs;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Backlogs
{
    public class BacklogTest
    {
        [Fact]
        public void CanAddABacklogTask()
        { 
            Backlog backlog = new Backlog();
            BacklogTask backlogTask = new BacklogTask();
            backlog.AddBacklogTask(backlogTask);
            
            Assert.Equal(backlogTask, backlog.GetBacklogTask(0)); 
        }

        [Fact]
        public void CanRemoveABacklogTask()
        {
            
            Backlog backlog = new Backlog();
            BacklogTask backlogTask = new BacklogTask();
            backlog.AddBacklogTask(backlogTask);
            Assert.Equal(backlogTask, backlog.GetBacklogTask(0)); 
            
            backlog.RemoveBacklogTask(backlogTask);
            Assert.Null(backlog.GetBacklogTask(0));
        }

        [Fact]
        public void CanGetAllBacklogTasks()
        {
            
            Backlog backlog = new Backlog();
            BacklogTask backlogTask1 = new BacklogTask();
            BacklogTask backlogTask2 = new BacklogTask();
            backlog.AddBacklogTask(backlogTask1);
            backlog.AddBacklogTask(backlogTask2);

            List<BacklogTask> backlogTasks = backlog.GetBacklogTasks();
            
            Assert.Equal(backlogTask1, backlogTasks.ElementAtOrDefault(0));
            Assert.Equal(backlogTask2, backlogTasks.ElementAtOrDefault(1));
        }
    }
   
}