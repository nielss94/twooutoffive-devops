using TwoOutOfFiveDevOps.Core.Backlogs;
using TwoOutOfFiveDevOps.Core.Backlogs.States;
using Xunit;

namespace TwoOutOfFiveDevOps.Test.Backlogs
{
    public class BacklogTaskStateTest
    {
        [Fact]
        public void BacklogTaskHasCreatedStateWhenInitialized()
        {
            BacklogTask backlogTask = new BacklogTask();

            Assert.IsType<BacklogTaskCreatedState>(backlogTask.GetState());
        }

        [Fact]
        public void CreatedBacklogTaskCanStart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();

            Assert.IsType<BacklogTaskInProgressState>(backlogTask.GetState());
        }
        
        [Fact]
        public void CreatedBacklogTaskCanNotFinish()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Finish();

            Assert.IsType<BacklogTaskCreatedState>(backlogTask.GetState());
        }
        
        [Fact]
        public void CreatedBacklogTaskCanNotRestart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Restart();

            Assert.IsType<BacklogTaskCreatedState>(backlogTask.GetState());
        }

        [Fact]
        public void InProgressBacklogTaskCanFinish()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Finish();

            Assert.IsType<BacklogFinishedState>(backlogTask.GetState());
        }
        
        [Fact]
        public void InProgressBacklogTaskCanNotFinishIfChildrenAreNotFinished()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask childTask = new BacklogTask();
            backlogTask.AddChild(childTask);
            
            childTask.Start();
            
            backlogTask.Start();
            backlogTask.Finish();

            Assert.IsType<BacklogTaskInProgressState>(backlogTask.GetState());
        }
        
        [Fact]
        public void InProgressBacklogTaskCanFinishIfChildrenAreFinished()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask childTask = new BacklogTask();
            backlogTask.AddChild(childTask);
            
            childTask.Start();
            childTask.Finish();
            
            backlogTask.Start();
            backlogTask.Finish();

            Assert.IsType<BacklogFinishedState>(backlogTask.GetState());
        }
        
        [Fact]
        public void InProgressBacklogTaskCanNotFinishIfChildrenOfChildrenAreNotFinished()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask childTask = new BacklogTask();
            BacklogTask childOfChildTask = new BacklogTask();
            backlogTask.AddChild(childTask);
            childTask.AddChild(childOfChildTask);
            
            childOfChildTask.Start();
            
            childTask.Start();
            childTask.Finish();
            
            backlogTask.Start();
            backlogTask.Finish();

            Assert.IsType<BacklogTaskInProgressState>(backlogTask.GetState());
        }
        
        [Fact]
        public void InProgressBacklogTaskCanFinishIfChildrenOfChildrenAreFinished()
        {
            BacklogTask backlogTask = new BacklogTask();
            BacklogTask childTask = new BacklogTask();
            BacklogTask childOfChildTask = new BacklogTask();
            backlogTask.AddChild(childTask);
            childTask.AddChild(childOfChildTask);
            
            childOfChildTask.Start();
            childOfChildTask.Finish();
            
            childTask.Start();
            childTask.Finish();
            
            backlogTask.Start();
            backlogTask.Finish();

            Assert.IsType<BacklogFinishedState>(backlogTask.GetState());
        }

        [Fact]
        public void InProgressBacklogTaskCanNotStart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Start();

            Assert.IsType<BacklogTaskInProgressState>(backlogTask.GetState());
        }
        
        [Fact]
        public void InProgressBacklogTaskCanNotRestart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Restart();

            Assert.IsType<BacklogTaskInProgressState>(backlogTask.GetState());
        }

        [Fact]
        public void FinishedBacklogTaskCanRestart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Finish();
            backlogTask.Restart();

            Assert.IsType<BacklogTaskCreatedState>(backlogTask.GetState());
        }

        [Fact]
        public void FinishedBacklogTaskCanNotStart()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Finish();
            backlogTask.Start();

            Assert.IsType<BacklogFinishedState>(backlogTask.GetState());
        }

        [Fact]
        public void FinishedBacklogTaskCanNotFinish()
        {
            BacklogTask backlogTask = new BacklogTask();
            backlogTask.Start();
            backlogTask.Finish();
            backlogTask.Finish();

            Assert.IsType<BacklogFinishedState>(backlogTask.GetState());
        }
    }
}